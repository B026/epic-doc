#include "GetFilesInDirectory.hpp"

void getFilesInDirectory(const std::string &directory, const std::string &extension, std::vector <std::string> &files)
{
	for (auto &file : fs::directory_iterator(directory))
	{
		if (fs::is_directory(file))
			getFilesInDirectory(file.path().string(), extension, files);
		else if (file.path().extension() == extension || file.path().extension() == ".h")
			files.emplace_back(file.path().string());
	}
}
