#include "Scope.hpp"
#include "ConfigFile.hpp"
#include "HTMLFile.hpp"
#include "MDInterpreter.hpp"
#include <fstream>
#include <regex>

//////////////////////////////
Scope::Scope(const std::string &scopeNameLine, const std::string &doc, Scope *parentScope) : doc(doc), parentScope(parentScope)
{
	/// Get the name
	std::regex scopeRegEx("(\t| )*(class|struct) (\\w+)(.*)$");
	std::smatch match;

	if (! std::regex_match(scopeNameLine, match, scopeRegEx))
		return;

	type = (match [2] == "class") ? Class : Struct;
	currentAccessLevel = (type == Class) ? AccessLevel::Private : AccessLevel::Public;

	name = match [3];

	/// Get supers
	std::string supersString(match [4]);

	if (supersString.empty())
		return;

	std::regex superRegEx(" *(:|,) *(\\w+) ((\\w+::)?(\\w+)(<.*>)?)(.*)?");

	while (std::regex_match(supersString, match, superRegEx))
	{
		supers.push_back(match [3]);
		supersString = match [7];
	}
}

//////////////////////////////
const std::string & Scope::getName() const
{
	return name;
}

//////////////////////////////
Scope * Scope::getParentScope()
{
	return parentScope;
}

//////////////////////////////
void Scope::setAccessLevel(const std::string &accessLevel)
{
	if (accessLevel == "public")
		currentAccessLevel = AccessLevel::Public;
	else if (accessLevel == "protected")
		currentAccessLevel = AccessLevel::Protected;
	else
		currentAccessLevel = AccessLevel::Private;
}

//////////////////////////////
std::string Scope::getTypeAsString() const
{
	switch (type)
	{
		case Class: return "class";
		case Struct: return "struct";
	}
}

//////////////////////////////
void Scope::addProperty(const std::string &name, const std::string &type, const std::string &doc)
{
	properties.emplace_back(name, type, doc);
	properties.back().setAccessLevel(currentAccessLevel);
}

//////////////////////////////
void Scope::addMethod(const std::string &returnType, const std::string &name, const std::string &params, const std::string &extras,
						 const std::string &doc)
{
	int methodId = 0;

	/// Check if the method is overloaded
	for (auto &method : methods)
	{
		if (method.getName() == name)
		{
			methodId = method.getId() + 1;
			break;
		}
	}

	methods.emplace_back(returnType, name, params, extras, doc, methodId);
	methods.back().setAccessLevel(currentAccessLevel);
}

//////////////////////////////
void Scope::addEnum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks)
{
	enums.emplace_back(name, doc, block, globalLinks);
}

//////////////////////////////
void Scope::addScope(const std::string &nameLine, const std::string &doc)
{
	scopes.emplace_back(nameLine, doc, this);
}

//////////////////////////////
Scope * Scope::getCurrentScope()
{
	if (scopes.empty())
		return nullptr;

	return &scopes.back();
}

//////////////////////////////
void Scope::propagateLinks(const std::vector <Link> &globalLinks, const std::vector <Link> &types)
{
	std::vector <Link> localLinks;
	putLocalLinks(localLinks);

	for (auto &enum_ : enums)
		enum_.propagateLinks(globalLinks);

	for (auto &property : properties)
		property.propagateLinks(globalLinks, types, localLinks);

	for (auto &method : methods)
		method.propagateLinks(globalLinks, types, localLinks);

	for (auto &link : globalLinks)
		link.replace(doc);

	for (auto &link : localLinks)
		link.replace(doc);

	for (auto &scope : scopes)
		scope.propagateLinks(globalLinks, types);
}

//////////////////////////////
void Scope::writeHTMLFile() const
{
	std::vector <Link> localLinks;
	putLocalLinks(localLinks);

	HTMLFile htmlFile(ConfigFile::outputDirectory + name + ".html");
	htmlFile.addElement("h1", name, name);

	/// Doc
	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));
	htmlFile.addElement("hr");

	/// List of enums
	if (enums.size())
	{
		htmlFile.addElement("h2", "Enums");
		auto &ul = htmlFile.addElement("ul");
		for (auto &enum_ : enums)
		{
			auto &li = ul.addElement("li");
			li.addElement("a", enum_.getName()).setAttribute("href", enum_.getName() + ".html");
		}
		htmlFile.addElement("hr");
	}

	/// list of properties
	if (properties.size())
	{
		htmlFile.addElement("h2", "Properties");

		std::vector <std::string> headers;
		headers.emplace_back("Name");
		headers.emplace_back("Type");

		auto &tableElement = htmlFile.addTable(headers);
		tableElement.setAttribute("class", "properties-table");
		for (auto &property : properties)
		{
			auto &rowElement = tableElement.addElement("tr");
			rowElement.addElement("td").addElement("a", property.getName()).setAttribute("href", "#" + property.getName());
			rowElement.addElement("td", property.getType());
		}

		htmlFile.addElement("hr");
	}

	/// list of methods
	if (methods.size())
	{
		htmlFile.addElement("h2", "Methods");

		std::vector <std::string> headers;
		headers.emplace_back("Return");
		headers.emplace_back("Name");

		auto &tableElement = htmlFile.addTable(headers);
		tableElement.setAttribute("class", "methods-table");
		for (auto &method : methods)
		{
			auto &rowElement = tableElement.addElement("tr");
			rowElement.addElement("td", method.getReturnType());
			auto &nameElement = rowElement.addElement("td");
			nameElement.addElement("a", method.getName()).setAttribute("href", "#" + method.getName() + std::to_string(method.getId()));
			nameElement.addElement("span", "(" + method.getParams() + ")" + method.getExtras());
		}

		htmlFile.addElement("hr");
	}

	/// List of internal scopes
	if (scopes.size())
	{
		htmlFile.addElement("h2", "Internal structs and classes");
		auto &ul = htmlFile.addElement("ul");
		for (auto &scope : scopes)
		{
			auto &li = ul.addElement("li");
			li.addElement("a", scope.getName()).setAttribute("href", scope.getName() + ".html");
			li.addElement("span", " : " + scope.getTypeAsString());
		}
		htmlFile.addElement("hr");
	}

	/// Enum detailed doc
	for (auto &enum_ : enums)
		enum_.writeHTMLFile();

	/// Properties detailed doc
	for (auto &property : properties)
		property.writeHTMLElement(htmlFile);

	/// Methods detailed doc
	for (auto &method : methods)
		method.writeHTMLElement(htmlFile);

	htmlFile.write();

	for (auto &scope : scopes)
		scope.writeHTMLFile();
}

//////////////////////////////
void Scope::putLocalLinks(std::vector <Link> &links) const
{
	for (auto &property : properties)
		links.emplace_back(property.getName(), "#" + property.getName());

	for (auto &method : methods)
		links.emplace_back(method.getName(), "#" + method.getName() + std::to_string(method.getId()));

	for (auto &scope : scopes)
		links.emplace_back(scope.getName(), scope.getName() + ".html");
}
