#include "AccessLevel.hpp"

//////////////////////////////
std::string accessLevelToString(AccessLevel accessLevel)
{
	switch (accessLevel)
	{
		case AccessLevel::Public: return "public";
		case AccessLevel::Protected: return "protected";
		case AccessLevel::Private: return "private";
		case AccessLevel::None: return "";
	}
}
