#include "HTMLElement.hpp"

//////////////////////////////
HTMLElement::Attribute::Attribute(const std::string &name, const std::string &value) : name(name), value(value)
{
}

//////////////////////////////
HTMLElement::HTMLElement(HTMLElement *parent) : parent(parent)
{
}

//////////////////////////////
HTMLElement::HTMLElement(const std::string &name, const std::string &innerHTML, HTMLElement *parent) : name(name), parent(parent)
{
	setInnerHTML(innerHTML);
}

//////////////////////////////
void HTMLElement::setName(const std::string &name)
{
	this->name = name;
}

//////////////////////////////
const std::string & HTMLElement::getName() const
{
	return name;
}

//////////////////////////////
void HTMLElement::setInnerHTML(const std::string &innerHTML)
{
	if (innerHTML.empty())
		return;

	if (name.empty())
		this->innerHTML = innerHTML;
	else
		addElement("", innerHTML);
}

//////////////////////////////
std::string * HTMLElement::getInnerHTML()
{
	return &innerHTML;
}

//////////////////////////////
void HTMLElement::setAttribute(const std::string &attributeName, const std::string &attributeValue)
{
	for (auto &attribute : attributes)
	{
		if (attribute.name == attributeName)
		{
			attribute.value = attributeValue;
			return;
		}
	}

	attributes.emplace_back(attributeName, attributeValue);
}

//////////////////////////////
void HTMLElement::setParent(HTMLElement *parent)
{
	this->parent = parent;
}

//////////////////////////////
HTMLElement * HTMLElement::getParent()
{
	return parent;
}

//////////////////////////////
HTMLElement & HTMLElement::addElement(const std::string &childName, const std::string &childInnerHTML)
{
	children.emplace_back(childName, childInnerHTML, this);
	return children.back();
}

//////////////////////////////
HTMLElement * HTMLElement::getElement(const std::string &childName)
{
	for (auto &child : children)
		if (child.name == childName)
			return &child;

	return nullptr;
}

//////////////////////////////
std::vector <HTMLElement> HTMLElement::getChildren()
{
	return children;
}

//////////////////////////////
std::string HTMLElement::toString(int tabLevel) const
{
	std::string tabs;
	for (int i = 0; i < tabLevel; i++)
		tabs += "\t";

	if (! name.size())
		return tabs + innerHTML;

	std::string output = tabs + "<" + name;

	for (auto &attribute : attributes)
		output += " " + attribute.name + "=\"" + attribute.value + "\"";

	if (! children.size())
	{
		output += " />";
		return output;
	}

	output += ">\n";

	for (auto &child : children)
	{
		output += child.toString(tabLevel + 1);
		output += "\n";
	}

	output += tabs + "</" + name + ">";
	return output;
}

//////////////////////////////
void HTMLElement::propagateLinks(const std::vector <Link> &globalLinks)
{
	// If node is innerHTML, replace link in itself
	if (name.empty())
	{
		for (auto &link : globalLinks)
			link.replace(innerHTML);
	}

	// Replace links recursively in its children
	for (auto &child : children)
		child.propagateLinks(globalLinks);
}
