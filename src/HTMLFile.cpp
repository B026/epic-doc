#include "HTMLFile.hpp"
#include "ConfigFile.hpp"
#include <iostream>
#include <fstream>

//////////////////////////////
HTMLFile::HTMLFile(const std::string &filename) : filename(filename)
{
	htmlElement.setName("html");

	/// Head element
	auto &headElement = htmlElement.addElement("head");
	headElement.addElement("meta").setAttribute("charset", "utf-8");

	auto &viewportMetaElement = headElement.addElement("meta");
	viewportMetaElement.setAttribute("name", "viewport");
	viewportMetaElement.setAttribute("content", "width=device-width, initial-scale=1.0");

	// CSS files
	for (auto &stylesheetFile : ConfigFile::stylesheets)
	{
		auto &linkElement = headElement.addElement("link");
		linkElement.setAttribute("rel", "stylesheet");
		linkElement.setAttribute("type", "text/css");
		linkElement.setAttribute("href", stylesheetFile);
	}

	/// Body element
	auto &bodyElement = htmlElement.addElement("body", ConfigFile::header);
	bodyElement.addElement("main");
	bodyElement.addElement("footer", ConfigFile::footer);
	mainElement = bodyElement.getElement("main");
}

//////////////////////////////
HTMLElement & HTMLFile::addElement(const std::string &elementName, const std::string &innerHTML, const std::string &id)
{
	return mainElement->addElement(elementName, innerHTML);
}

//////////////////////////////
HTMLElement & HTMLFile::addTable(const std::vector <std::string> &headers)
{
	auto &tableElement = mainElement->addElement("table");
	auto &headersElement = tableElement.addElement("tr");

	for (auto &header : headers)
		headersElement.addElement("th", header);

	return tableElement;
}

//////////////////////////////
HTMLElement * HTMLFile::getMainElement()
{
	return mainElement;
}

//////////////////////////////
void HTMLFile::propagateLinks(const std::vector <Link> &globalLinks)
{
	mainElement->propagateLinks(globalLinks);
}

//////////////////////////////
void HTMLFile::write() const
{
	std::ofstream file(filename);

	if (! file.is_open())
	{
		std::cout << "Error writting html file, unable to open file: " << filename << std::endl;
		return;
	}

	file << "<!DOCTYPE html>\n";
	file << htmlElement.toString() << std::endl;
	file.close();
}
