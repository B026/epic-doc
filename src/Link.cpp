#include "Link.hpp"

//////////////////////////////
Link::Link(const std::string &text, const std::string &href) : text(text), href(href)
{
}

//////////////////////////////
void Link::replace(std::string &in) const
{
	int textLength = text.length();
	std::string replacementText("<a href=\"" + href + "\">" + text + "</a>");

	for (int i = 0; i < in.length(); i++)
	{
		if (in.length() < i + textLength)
			return;

		if (in [i] == text [0])
		{
			// Look behind for word boundary
			char prevChar = (i) ? in [i - 1] : ' ';
			if (prevChar != ' ' && prevChar != ';' && prevChar != '<' && prevChar != '.' && prevChar != ',' && prevChar != '\t' && prevChar != '\n')
				continue;

			// Look forward for word boundary
			char nextChar = (i + textLength < in.length()) ? in [i + textLength] : ' ';
			if (nextChar != ' ' && nextChar != '(' && nextChar != '&' && nextChar != '>' && nextChar != '.' && nextChar != ',' && nextChar != '\t' &&
				nextChar != '\n' && nextChar != ';')
				continue;

			// Test if it matches
			bool match = true;
			for (int j = 1; j < textLength; j++)
			{
				if (in [i + j] != text [j])
				{
					match = false;
					break;
				}
			}

			if (match)
			{
				in.erase(i, textLength);
				in.insert(i, replacementText);
				i += replacementText.length();
			}
		}
	}
}
