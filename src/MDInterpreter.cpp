#include "MDInterpreter.hpp"
#include <cctype>
#include <vector>
#include <fstream>
#include <iostream>

/////////////////////////////////////////////
void MDInterpreter::mdToHTML(const std::string &md, HTMLElement &htmlElement)
{
	std::vector <std::string> rawLines;
	MDInterpreter interpreter = getInstance();

	// Split md string by \n
	std::string rawLine;
	for (auto &c : md)
	{
		if (c == '\n')
		{
			if (rawLine.front() == ' ')
				rawLine.erase(0, 1);

			if (! rawLine.empty())
			{
				rawLines.emplace_back(rawLine);
				rawLine.clear();
			}
		}
		else
		{
			rawLine.push_back(c);
		}
	}

	// Interpret rawLines
	HTMLElement *currentHTMLElement = &htmlElement;
	std::smatch match;
	bool prevLineLi = false;

	for (auto &line : rawLines)
	{
		if (std::regex_match(line, match, interpreter.liRegEx)) // Test if it's a li element
		{
			if (! prevLineLi)
			{
				currentHTMLElement = &currentHTMLElement->addElement((isdigit(std::string(match [1]).front()) ? "ol" : "ul"));
				prevLineLi = true;
			}

			MDInterpreter::createElement("li", *currentHTMLElement, match [2]);
		}
		else // It's a paragraph element
		{
			if (prevLineLi)
				currentHTMLElement = currentHTMLElement->getParent();

			MDInterpreter::createElement("p", *currentHTMLElement, line);
			prevLineLi = false;
		}
	}
}

/////////////////////////////////////////////
void MDInterpreter::mdFileToHTMLFile(const std::string &filename, HTMLFile &htmlFile)
{
	std::ifstream mdFile(filename);

	if (! mdFile.is_open())
	{
		std::cout << "Can't open md file\n";
		return;
	}

	LineType prevLine = Empty;

	std::string line;
	std::string innerHTML;
	std::regex headerRegEx("(#+)( |\t)+([^#]+)(#*)?");
	std::regex liRegEx("([\\+\\*-]|\\d+\\.)[ \t]+(.*)");
	std::regex noteRegEx("> (.*)");
	std::smatch match;
	HTMLElement *parentElement = htmlFile.getMainElement();

	while (std::getline(mdFile, line))
	{
		if (line.empty())
		{
			parentElement = analizePrevLine(&prevLine, parentElement, innerHTML);
			innerHTML.clear();
		}
		else
		{
			if (std::regex_match(line, match, headerRegEx))
			{
				parentElement = analizePrevLine(&prevLine, parentElement, innerHTML);
				innerHTML.clear();

				createElement("h" + std::to_string(std::string(match [1]).length()), *parentElement, match [3]);
				prevLine = Empty;
			}
			else if (std::regex_match(line, match, liRegEx))
			{
				if (prevLine != Li)
					parentElement = analizePrevLine(&prevLine, parentElement, innerHTML);

				if (prevLine == Empty)
				{
					parentElement = &parentElement->addElement((isdigit(std::string(match [1]) [0])) ? "ol" : "ul");
					innerHTML = match [2];
					prevLine = Li;
				}
				else if (prevLine == Li)
				{
					createElement("li", *parentElement, innerHTML);
					innerHTML = match [2];
				}
			}
			else if (std::regex_match(line, match, noteRegEx))
			{
				if (prevLine != Note)
					parentElement = analizePrevLine(&prevLine, parentElement, innerHTML);

				if (prevLine == Empty)
				{
					innerHTML = match [1];
					prevLine = Note;
				}
				else
				{
					innerHTML += " ";
					innerHTML += match [1];
				}
			}
			else
			{
				if (prevLine == Empty)
				{
					innerHTML = line;
					prevLine = Paragraph;
				}
				else
				{
					innerHTML += " ";
					innerHTML += line;
				}
			}
		}
	}

	analizePrevLine(&prevLine, parentElement, innerHTML);
	mdFile.close();
}

/////////////////////////////////////////////
void MDInterpreter::createElement(const std::string &elementName, HTMLElement &parent, std::string innerHTML)
{
	MDInterpreter interpreter = getInstance();
	innerHTML = std::regex_replace(innerHTML, interpreter.boldRegEx, "<strong>$2</strong>");
	innerHTML = std::regex_replace(innerHTML, interpreter.italicsRegEx, "<em>$2</em>");
	innerHTML = std::regex_replace(innerHTML, interpreter.strikethroughRegEx, "<del>$1</del>");
	innerHTML = std::regex_replace(innerHTML, interpreter.imageRegEx, "<img alt='$1' src='$2' />");
	innerHTML = std::regex_replace(innerHTML, interpreter.linkRegEx, "<a href='$2'>$1</a>");
	parent.addElement(elementName, innerHTML);
}

/////////////////////////////////////////////
HTMLElement * MDInterpreter::analizePrevLine(LineType *prevLineType, HTMLElement *parentElement, const std::string &innerHTML)
{
	switch (*prevLineType)
	{
		case Paragraph:
			createElement("p", *parentElement, innerHTML);
		break;

		case Note:
			createElement("blockquote", *parentElement, innerHTML);
		break;

		case Li:
			createElement("li", *parentElement, innerHTML);
			*prevLineType = Empty;
			return parentElement->getParent();
	}

	*prevLineType = Empty;
	return parentElement;
}

/////////////////////////////////////////////
MDInterpreter::MDInterpreter()
{
	boldRegEx.assign("(__|\\*\\*)(.*?)(__|\\*\\*)");
	imageRegEx.assign("!\\[(.*?)\\]\\((.*?)\\)");
	italicsRegEx.assign("(_|\\*)(.*?)(_|\\*)");
	liRegEx.assign("^([\\+\\*-]|\\d+\\.)[ \t]+(.*)$");
	linkRegEx.assign("\\[(.*?)\\]\\((.*?)\\)");
	strikethroughRegEx.assign("--(.*?)--");
}

/////////////////////////////////////////////
MDInterpreter & MDInterpreter::getInstance()
{
	static MDInterpreter interpreter;
	return interpreter;
}
