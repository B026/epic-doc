#include "ConfigFile.hpp"
#include "GetFilesInDirectory.hpp"
#include "Project.hpp"
#include <iostream>

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		std::cout << "Config file's location not specified\n";
		return 0;
	}

	if (! ConfigFile::read(argv [1]))
		return 0;

	/// Get input files
	std::vector <std::string> files;
	getFilesInDirectory(ConfigFile::inputDirectory, ".hpp", files);

	Project project;
	for (auto &file : files)
		project.readFile(file);

	for (auto &file : ConfigFile::mdFiles)
		project.readMDFile(file);

	project.propagateLinks();
	project.writeHTMLDoc();

	return 0;
}
