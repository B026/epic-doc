#include "Function.hpp"
#include "ConfigFile.hpp"
#include "MDInterpreter.hpp"
#include <regex>

//////////////////////////////
Function::Function(const std::string &returnType, const std::string &name, const std::string &params, const std::string &extras,
			   const std::string &doc, int id) : name(name), extras(extras), id(id), accessLevel(AccessLevel::None)
{
	this->returnType = std::regex_replace(returnType, std::regex("<(.*)>"), "&lt;$1&gt;");
	this->params = std::regex_replace(params, std::regex("<(.*)>"), "&lt;$1&gt;");
	this->doc = std::regex_replace(doc, std::regex("<(.*)>"), "&lt;$1&gt;");
}

//////////////////////////////
void Function::setAccessLevel(AccessLevel accessLevel)
{
	this->accessLevel = accessLevel;
}

//////////////////////////////
const std::string & Function::getReturnType() const
{
	return returnType;
}

//////////////////////////////
const std::string & Function::getName() const
{
	return name;
}

//////////////////////////////
const std::string & Function::getParams() const
{
	return params;
}

//////////////////////////////
const std::string & Function::getExtras() const
{
	return extras;
}

//////////////////////////////
int Function::getId() const
{
	return id;
}

//////////////////////////////
void Function::propagateLinks(const std::vector <Link> &globalLinks, const std::vector <Link> &types, const std::vector <Link> &localLinks)
{
	for (auto &link : globalLinks)
		link.replace(doc);

	for (auto &link : localLinks)
		link.replace(doc);

	for (auto &type : types)
	{
		type.replace(returnType);
		type.replace(params);
	}
}

//////////////////////////////
void Function::writeHTMLElement(HTMLFile &htmlFile) const
{
	auto &headingElement = htmlFile.addElement("h3");
	headingElement.setAttribute("id", name + std::to_string(id));
	headingElement.addElement("span", returnType);
	headingElement.addElement("strong", name);
	headingElement.addElement("span", "(" + params + ")" + extras);

	if (accessLevel != AccessLevel::None)
		headingElement.addElement("code", "[" + accessLevelToString(accessLevel) + "]");

	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));

	htmlFile.addElement("hr");
}

//////////////////////////////
void Function::writeHTMLFile() const
{
	HTMLFile htmlFile(ConfigFile::outputDirectory + name + ".html");
	htmlFile.addElement("h1", name + "()");
	writeHTMLElement(htmlFile);
	htmlFile.write();
}
