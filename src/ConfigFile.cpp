#include "ConfigFile.hpp"
#include "GetFilesInDirectory.hpp"
#include <iostream>
#include <fstream>
#include <regex>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

//////////////////////////////
std::string ConfigFile::projectName;
std::string ConfigFile::inputDirectory;
std::string ConfigFile::outputDirectory;
std::vector <std::string> ConfigFile::stylesheets;
std::string ConfigFile::header;
std::string ConfigFile::footer;
std::vector <std::string> ConfigFile::mdFiles;

//////////////////////////////
bool ConfigFile::read(std::string filename)
{
	/// Get config file name
	if (fs::is_directory(filename))
	{
		bool foundConfigFile = false;
		for (auto &directory : fs::directory_iterator(filename))
		{
			if (directory.path() == filename + "/epic-doc.conf")
			{
				filename = directory.path();
				foundConfigFile = true;
				break;
			}
		}

		if (! foundConfigFile)
		{
			std::cout << "Config file doesn't exist in the given path\n";
			return false;
		}
	}

	std::ifstream configFile(filename);

	if (! configFile.is_open())
	{
		std::cout << "Unable to open config file\n";
		return false;
	}

	std::string line;
	std::smatch match;

	std::regex projectNameRegEx("(\t| )*project-name:(\t| )*(.*)");
	std::regex inputDirectoryRegEx("(\t| )*input-directory:(\t| )*(.*)");
	std::regex outputDirectoryRegEx("(\t| )*output-directory:(\t| )*(.*)");
	std::regex stylesheetDirectoryRegex("(\t| )*stylesheet-directory:(\t| )*(.*)");
	std::regex headerFilenameRegEx("(\t| )*header-filename:(\t| )*(.*)");
	std::regex footerFilenameRegEx("(\t| )*footer-filename:(\t| )*(.*)");
	std::regex mdDirectoryRegEx("(\t| )*md-directory:(\t| )*(.*)");

	while (std::getline(configFile, line))
	{
		if (std::regex_match(line, match, projectNameRegEx))
			ConfigFile::projectName = match [3];
		else if (std::regex_match(line, match, inputDirectoryRegEx))
			ConfigFile::inputDirectory = match [3];
		else if (std::regex_match(line, match, outputDirectoryRegEx))
			ConfigFile::outputDirectory = match [3];
		else if (std::regex_match(line, match, stylesheetDirectoryRegex))
			getFilesInDirectory(match [3], ".css", ConfigFile::stylesheets);
		else if (std::regex_match(line, match, headerFilenameRegEx))
		{
			std::ifstream headerFile(match [3]);
			if (headerFile.is_open())
			{
				std::string aux;
				while (std::getline(headerFile, aux))
					header += aux;
			}

		}
		else if (std::regex_match(line, match, footerFilenameRegEx))
		{
			std::ifstream footerFile(match [3]);
			if (footerFile.is_open())
			{
				std::string aux;
				while (std::getline(footerFile, aux))
					footer += aux;
			}
		}
		else if (std::regex_match(line, match, mdDirectoryRegEx))
			getFilesInDirectory(match [3], ".md", ConfigFile::mdFiles);
	}

	if (inputDirectory.back() != '/')
		inputDirectory += "/";

	if (outputDirectory.back() != '/')
		outputDirectory += "/";

	return true;
}

//////////////////////////////
ConfigFile::ConfigFile()
{
}

//////////////////////////////
ConfigFile::~ConfigFile()
{
}
