#include "Project.hpp"
#include "ConfigFile.hpp"
#include "MDInterpreter.hpp"
#include <iostream>
#include <fstream>

//////////////////////////////
Project::Project() : currentScope(nullptr)
{
	docCommentRegEx.assign("^(\t| )*//+ ?(.*)$");
	scopeRegEx.assign("(\t| )*(class|struct) (\\w+)( : .*)?$");
	accessLevelRegEx.assign("(\t| )*(public|protected|private):(.*)");
	variableRegEx.assign("(\t| )*((\\w+)?(\\w+::)?(\\w+)( ?<.*>)?( ?(\\*|&)? ?))(\\w+);");
	functionRegEx.assign("(\t| )*((.*? ?|~)(\\w+)\\((.*)\\)([^;{]*))(;|\\{)?");
	enumRegEx.assign("(\t| )*enum( class)? (\\w+).*");
	blockEndRegEx.assign(".*};.*");
	liRegEx.assign("(\\*|\\+|-|\\d+\\.) (.*)");
}

//////////////////////////////
void Project::readFile(const std::string &filename)
{
	/// State machine
	enum State
	{
		Normal,
		ReadingCommentBlock,
		ReadingEnum
	};

	State state = Normal;

	/// Try to open file
	std::ifstream file(filename);

	if (! file.is_open())
	{
		file.close();
		std::cout << "Error openning file: " << filename << std::endl;
		return;
	}

	std::smatch match;

	/// Strings used to read the file
	bool prevLineEmpty = false;
	std::string line;
	std::string textDoc;
	std::string blockStartLine;
	std::string block;

	/// Read the file
	while (std::getline(file, line))
	{
		switch (state)
		{
			case Normal:
				if (std::regex_match(line, match, docCommentRegEx))
				{
					textDoc = match [2];
					state = ReadingCommentBlock;
				}
				else if (std::regex_match(line, match, accessLevelRegEx) && currentScope) /// AccessLevel
				{
					currentScope->setAccessLevel(match [2]);
				}
				else if (std::regex_match(line, blockEndRegEx) && currentScope)
				{
					currentScope = currentScope->getParentScope();
				}
			break;

			case ReadingCommentBlock:
				if (std::regex_match(line, match, docCommentRegEx))
				{
					std::string commentText(match [2]);

					if (commentText.empty())
					{
						textDoc += "\n";
						prevLineEmpty = true;
					}
					else
					{
						/// Test if it's a li element
						if (std::regex_match(commentText, liRegEx))
							textDoc += "\n";

						if (! prevLineEmpty)
							textDoc += " ";

						textDoc += match [2];
						prevLineEmpty = false;
					}
				}
				else if (std::regex_match(line, match, scopeRegEx))	/// Scopes
				{
					addLink(match [3]);
					addType(match [3]);

					if (currentScope)
					{
						currentScope->addScope(line, textDoc);
						currentScope = currentScope->getCurrentScope();
					}
					else
					{
						addScope(line, textDoc);
					}

					textDoc.clear();
					prevLineEmpty = false;
					state = Normal;
				}
				else if (std::regex_match(line, match, variableRegEx))	/// Properties and variables
				{
					if (currentScope)
						currentScope->addProperty(match [9], match [2], textDoc);
					else
						addVariable(match [9], match [2], textDoc);

					addLink(match [9]);
					textDoc.clear();
					prevLineEmpty = false;
					state = Normal;
				}
				else if (std::regex_match(line, match, functionRegEx))	/// Methods and functions
				{
					if (currentScope)
						currentScope->addMethod(match [3], match [4], match [5], match [6], textDoc);
					else
						addFunction(match [3], match [4], match [5], match [6], textDoc);

					addLink(match [4]);
					textDoc.clear();
					prevLineEmpty = false;
					state = Normal;
				}
				else if (std::regex_match(line, match, enumRegEx)) /// Enums
				{
					blockStartLine = line;
					state = ReadingEnum;
				}
				else
				{
					state = Normal;
					prevLineEmpty = false;
					textDoc.clear();
				}
			break;

			case ReadingEnum:
				if (std::regex_match(line, blockEndRegEx))
				{
					if (std::regex_match(blockStartLine, match, enumRegEx)) /// Enum
					{
						std::string enumName(match [3]);

						if (currentScope)
							currentScope->addEnum(enumName, textDoc, block, links);
						else
							addEnum(enumName, textDoc, block, links);

						addLink(enumName);
						addType(enumName);
					}

					textDoc.clear();
					blockStartLine.clear();
					block.clear();
					state = Normal;
				}
				else
				{
					block += line + "\n";
				}
		}
	}

	file.close();
}

//////////////////////////////
void Project::readMDFile(const std::string &filename)
{
	std::regex filenameRegEx("(\\/.*)?\\/(.*)\\..*");
	std::smatch match;

	if (! std::regex_match(filename, match, filenameRegEx))
		return;

	std::string htmlFilename(ConfigFile::outputDirectory + std::string(match [2]) + ".html");
	mdFiles.emplace_back(htmlFilename);
	MDInterpreter::mdFileToHTMLFile(filename, mdFiles.back());
}

//////////////////////////////
void Project::propagateLinks()
{
	for (auto &enum_ : enums)
		enum_.propagateLinks(links);

	for (auto &variable : variables)
		variable.propagateLinks(links, types);

	for (auto &function : functions)
		function.propagateLinks(links, types);

	for (auto &scope : scopes)
		scope.propagateLinks(links, types);

	for (auto &mdFile : mdFiles)
		mdFile.propagateLinks(links);
}

//////////////////////////////
void Project::writeHTMLDoc() const
{
	for (auto &enum_ : enums)
		enum_.writeHTMLFile();

	for (auto &variable : variables)
		variable.writeHTMLFile();

	for (auto &function : functions)
		function.writeHTMLFile();

	for (auto &scope : scopes)
		scope.writeHTMLFile();

	for (auto &mdFile : mdFiles)
		mdFile.write();

	writeAPIReferenceLists();
}

//////////////////////////////
void Project::writeAPIReferenceLists() const
{
	HTMLFile apiReference(ConfigFile::outputDirectory + "api-reference.html");

	// Enums
	if (enums.size())
	{
		apiReference.addElement("h2", "Enums");
		auto &enumsList = apiReference.addElement("ul");

		for (auto &_enum : enums)
			enumsList.addElement("li").addElement("a", _enum.getName()).setAttribute("href", _enum.getName() + ".html");
	}

	// Scopes
	if (scopes.size())
	{
		apiReference.addElement("h2", "Classes and Strucs");
		auto &scopesList = apiReference.addElement("ul");

		for (auto &scope : scopes)
			scopesList.addElement("li").addElement("a", scope.getName()).setAttribute("href", scope.getName() + ".html");
	}

	// Global variables
	if (variables.size())
	{
		apiReference.addElement("h2", "Global Variables");
		auto &variablesList = apiReference.addElement("ul");

		for (auto &variable : variables)
			variablesList.addElement("li").addElement("a", variable.getName()).setAttribute("href", variable.getName() + ".html");
	}

	// Functions
	if (functions.size())
	{
		apiReference.addElement("h2", "Functions");
		auto &functionsList = apiReference.addElement("ul");

		for (auto &function : functions)
			functionsList.addElement("li").addElement("a", function.getName()).setAttribute("href", function.getName() + ".html");
	}

	apiReference.write();
}

//////////////////////////////
void Project::addEnum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks)
{
	enums.emplace_back(name, doc, block, globalLinks);
}

//////////////////////////////
void Project::addVariable(const std::string &name, const std::string &type, const std::string &doc)
{
	variables.emplace_back(name, type, doc);
}

//////////////////////////////
void Project::addFunction(const std::string &returnType, const std::string &name, const std::string &params, const std::string &extras,
						  const std::string &doc)
{
	int functionId = 0;

	/// Check if the method is overloaded
	for (auto &function : functions)
	{
		if (function.getName() == name)
		{
			functionId = function.getId() + 1;
			break;
		}
	}

	functions.emplace_back(returnType, name, params, extras, doc, functionId);
}

//////////////////////////////
void Project::addScope(const std::string &nameLine, const std::string &doc)
{
	scopes.emplace_back(nameLine, doc, currentScope);
	currentScope = &scopes.back();
}

//////////////////////////////
void Project::addLink(const std::string &text)
{
	if (currentScope)
		links.emplace_back(currentScope->getName() + "::" + text, currentScope->getName() + ".html#" + text);
	else
		links.emplace_back(text, text + ".html");
}

//////////////////////////////
void Project::addType(const std::string &type)
{
	types.emplace_back(type, type + ".html");
}
