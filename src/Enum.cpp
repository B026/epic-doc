#include "Enum.hpp"
#include "ConfigFile.hpp"
#include "HTMLFile.hpp"
#include "MDInterpreter.hpp"
#include <regex>
#include <sstream>

//////////////////////////////
Enum::EnumElement::EnumElement(const std::string &name, const std::string &value, const std::string &doc) : name(name), value(value), doc(doc)
{
}

//////////////////////////////
Enum::Enum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks) : name(name), doc(doc)
{
	std::stringstream stream(block);
	std::string line;
	std::regex enumRegEx("(\t| )*(\\w+)(\t| )*(= (\\w+))?,?(\t| )*\\/\\/+(\t| )*(.*)");
	std::smatch match;

	while (std::getline(stream, line))
	{
		if (std::regex_match(line, match, enumRegEx))
		{
			std::string enumElement(match [2]);
			globalLinks.emplace_back(name + "::" + enumElement, name + ".html");
			addEnum(enumElement, match [5], match [8]);
		}
	}
}

//////////////////////////////
const std::string & Enum::getName() const
{
	return name;
}

//////////////////////////////
void Enum::addEnum(const std::string &name, const std::string &value, const std::string &doc)
{
	enums.emplace_back(name, value, doc);
}

//////////////////////////////
void Enum::propagateLinks(const std::vector <Link> &globalLinks)
{
	for (auto &link : globalLinks)
		link.replace(doc);
}

//////////////////////////////
void Enum::writeHTMLElement(HTMLFile &htmlFile) const
{
	htmlFile.addElement("h3", name).setAttribute("id", name);

	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));
	writeHTMLEnums(htmlFile);

	htmlFile.addElement("hr");
}

//////////////////////////////
void Enum::writeHTMLFile() const
{
	HTMLFile htmlFile(ConfigFile::outputDirectory + name + ".html");
	htmlFile.addElement("h1", name + " : enum");

	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));
	writeHTMLEnums(htmlFile);
	htmlFile.write();
}

//////////////////////////////
void Enum::writeHTMLEnums(HTMLFile &htmlFile) const
{
	std::vector <std::string> headers;
	headers.emplace_back("Constant");
	headers.emplace_back("Value");
	headers.emplace_back("Description");

	auto &tableElement = htmlFile.addTable(headers);
	for (auto &enumElement : enums)
	{
		auto &rowElement = tableElement.addElement("tr");
		rowElement.addElement("td", enumElement.name);
		rowElement.addElement("td", enumElement.value);
		rowElement.addElement("td", enumElement.doc);
	}
}
