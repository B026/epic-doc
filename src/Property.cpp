#include "Property.hpp"
#include "ConfigFile.hpp"
#include "MDInterpreter.hpp"
#include <regex>

//////////////////////////////
Property::Property(const std::string &name, const std::string &type, const std::string &doc) :
				   name(name), accessLevel(AccessLevel::None)
{
	this->type = std::regex_replace(type, std::regex("<(.*)>"), "&lt;$1&gt;");
	this->doc = std::regex_replace(doc, std::regex("<(.*)>"), "&lt;$1&gt;");
}

//////////////////////////////
const std::string & Property::getName() const
{
	return name;
}

//////////////////////////////
const std::string & Property::getType() const
{
	return type;
}

void Property::propagateLinks(const std::vector <Link> &globalLinks, const std::vector <Link> &types, const std::vector <Link> &localLinks)
{
	for (auto &typeLink : types)
		typeLink.replace(type);

	for (auto &globalLink : globalLinks)
		globalLink.replace(doc);

	for (auto &localLink : localLinks)
		localLink.replace(doc);
}

//////////////////////////////
void Property::setAccessLevel(AccessLevel accessLevel)
{
	this->accessLevel = accessLevel;
}

//////////////////////////////
void Property::writeHTMLElement(HTMLFile &htmlFile) const
{
	auto &headingElement = htmlFile.addElement("h3");
	headingElement.setAttribute("id", name);
	headingElement.addElement("strong", name);
	headingElement.addElement("span", " : " + type);

	if (accessLevel != AccessLevel::None)
		headingElement.addElement("code", "[" + accessLevelToString(accessLevel) + "]");

	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));
	htmlFile.addElement("hr");
}

//////////////////////////////
void Property::writeHTMLFile() const
{
	HTMLFile htmlFile(ConfigFile::outputDirectory + name + ".html");
	htmlFile.addElement("h1", name + " : " + type);

	MDInterpreter::mdToHTML(doc, htmlFile.addElement("div"));
	htmlFile.write();
}
