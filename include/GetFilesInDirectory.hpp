#ifndef DOXEN_GET_FILES_IN_DIRECTORY_HPP
#define DOXEN_GET_FILES_IN_DIRECTORY_HPP

#include <string>
#include <vector>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;

void getFilesInDirectory(const std::string &directory, const std::string &extension, std::vector <std::string> &files);

#endif // DOXEN_GET_FILES_IN_DIRECTORY_HPP
