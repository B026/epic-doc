#ifndef DOXEN_PROPERTY_HPP
#define DOXEN_PROPERTY_HPP

#include <string>
#include <vector>
#include "AccessLevel.hpp"
#include "HTMLFile.hpp"
#include "Link.hpp"

/////////////////////////////////////////////
// Holds the documentation of a class's (or structure)
// property or a variable.
/////////////////////////////////////////////
class Property
{
public:
	/////////////////////////////////////////////
	// Sets property's name, type and documentation.
	/////////////////////////////////////////////
	Property(const std::string &name, const std::string &type, const std::string &doc);

	/////////////////////////////////////////////
	// Gets property's name
	/////////////////////////////////////////////
	const std::string & getName() const;

	/////////////////////////////////////////////
	// Gets property's type
	/////////////////////////////////////////////
	const std::string & getType() const;

	void propagateLinks(const std::vector <Link> &globalLinks, const std::vector <Link> &types,
						const std::vector <Link> &localLinks = std::vector <Link>());

	/////////////////////////////////////////////
	// Sets property's access level when it's member
	// of a class or a structure.
	/////////////////////////////////////////////
	void setAccessLevel(AccessLevel accessLevel);

	/////////////////////////////////////////////
	// Writes property's documentation into an
	// HTML elements.
	//
	// Generates a h3 with property's name, a p
	// element for property's documentation and a
	// hr.
	/////////////////////////////////////////////
	void writeHTMLElement(HTMLFile &htmlFile) const;

	/////////////////////////////////////////////
	// Writes property's documentation into a HTML file.
	//
	// File's name is property's name + ".html". Generates
	// a h1 element which displays property's name
	// and a p element for property's documentation
	/////////////////////////////////////////////
	void writeHTMLFile() const;

private:
	/////////////////////////////////////////////
	// Holds property's access level when it's
	// part of a scope (a class or a struct).
	//
	// It's AccessLevel::None by default and can be
	// set using setAccessLevel()
	/////////////////////////////////////////////
	AccessLevel accessLevel;

	/////////////////////////////////////////////
	// Property's name.
	/////////////////////////////////////////////
	std::string name;

	/////////////////////////////////////////////
	// Property's name.
	/////////////////////////////////////////////
	std::string type;

	/////////////////////////////////////////////
	// Property's documentation.
	/////////////////////////////////////////////
	std::string doc;
};

#endif // DOXEN_PROPERTY_HPP
