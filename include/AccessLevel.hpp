#ifndef DOXEN_ACCESS_LEVEL_HPP
#define DOXEN_ACCESS_LEVEL_HPP

#include <string>

//////////////////////////////
// Defines possible access levels
// of methods and properties
//////////////////////////////
enum AccessLevel
{
	Public		= 0,	// Public
	Protected	= 1,	// Protected
	Private		= 2,	// Private
	None		= 3		// For functions and variables
};

//////////////////////////////
// Converts AccessLevel enum
// into a string. The returned
// string is equal to the name
// of the given access level.
//////////////////////////////
std::string accessLevelToString(AccessLevel accessLevel);

#endif // DOXEN_ACCESS_LEVEL_HPP
