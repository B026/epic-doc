#ifndef DOXEN_SCOPE_HPP
#define DOXEN_SCOPE_HPP

#include <vector>
#include <string>
#include "Enum.hpp"
#include "Property.hpp"
#include "Function.hpp"
#include "Link.hpp"

/////////////////////////////////////////////
// Holds documentation of a class or a struct.
// It has properties, methods, enums and scopes too.
/////////////////////////////////////////////
class Scope
{
public:
	/////////////////////////////////////////////
	// Defines the possibles type of scopes.
	/////////////////////////////////////////////
	enum Type
	{
		Class,	// If the scope is a class
		Struct	// If the scope is a struct
	};

	/////////////////////////////////////////////
	// Sets scope's documentation and parentScope.
	// Analizes scopeNameLine to determine scope's
	// type, name and supers (if it has).
	/////////////////////////////////////////////
	Scope(const std::string &scopeNameLine, const std::string &doc, Scope *parentScope = nullptr);

	/////////////////////////////////////////////
	// Gets scope's name
	/////////////////////////////////////////////
	const std::string & getName() const;

	/////////////////////////////////////////////
	// Gets scope's parent scope.
	/////////////////////////////////////////////
	Scope * getParentScope();

	/////////////////////////////////////////////
	// Sets scope's currentAccessLevel
	/////////////////////////////////////////////
	void setAccessLevel(const std::string &accessLevel);

	/////////////////////////////////////////////
	// Gets scope's type as a string.
	//
	// Returns "class" or "struct".
	/////////////////////////////////////////////
	std::string getTypeAsString() const;

	/////////////////////////////////////////////
	// Adds a property to the scope specifying
	// Property::name, Property::type and Property::doc.
	//
	// Property::accessLevel is determined by currentAccessLevel
	/////////////////////////////////////////////
	void addProperty(const std::string &name, const std::string &type, const std::string &doc);

	/////////////////////////////////////////////
	// Adds a method to the scope specifying Function::returnType,
	// Function::name, Function::params, Function::extras
	// and Function::doc.
	//
	// Funtion::accessLevel is determined by currentAccessLevel
	/////////////////////////////////////////////
	void addMethod(const std::string &returnType, const std::string &name, const std::string &params, const std::string &extras,
				   const std::string &doc);

	/////////////////////////////////////////////
	// Adds a enum to the scope specifying Enum::name,
	// Enum::doc and the block code which represents
	// enum's body.
	/////////////////////////////////////////////
	void addEnum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks);

	/////////////////////////////////////////////
	// Adds a scope to the scope specifying doc and
	// line code which holds scope's type, name and supers.
	//
	// The new scope becomes into the current scope,
	// so variables, functions, enums and scopes
	// readed by Project are added to it instead
	// of this.
	/////////////////////////////////////////////
	void addScope(const std::string &nameLine, const std::string &doc);

	/////////////////////////////////////////////
	// Gets the current scope (if there is)
	/////////////////////////////////////////////
	Scope * getCurrentScope();

	void propagateLinks(const std::vector <Link> &globalLinks, const std::vector <Link> &types);

	/////////////////////////////////////////////
	// Writes scope documentation into a HTML file.
	//
	// HTML file name is scope's name + ".html" and
	// also is displayed in a h1 element. A p element
	// is generated for scope's doc, after that
	// 4 ul are generated for enums, properties and
	// methods and scopes. Next to, detailed documentation
	// of enums, properties and methods are generated
	// in the same HTML file, but scopes' detailed
	// documentation is generated in separated HTML files.
	/////////////////////////////////////////////
	void writeHTMLFile() const;

private:
	/////////////////////////////////////////////
	// Insert links of enums, properties, methods and scopes
	// into the given links.
	/////////////////////////////////////////////
	void putLocalLinks(std::vector <Link> &links) const;

	/////////////////////////////////////////////
	// It's used to determine the access level of
	// members when are added.
	//
	// It's AccessLevel::Public for Struct scopes
	// and AccessLevel::Private for Class scopes.
	//
	// It's changed when Project finds a access level
	// code line using setAccessLevel()
	/////////////////////////////////////////////
	AccessLevel currentAccessLevel;

	/////////////////////////////////////////////
	// It's used to determine the default value of
	// currentAccessLevel and to determine if the
	// is a class or a struct.
	//
	// It's set in the constructor.
	/////////////////////////////////////////////
	Type type;

	/////////////////////////////////////////////
	// Scope's name
	/////////////////////////////////////////////
	std::string name;

	/////////////////////////////////////////////
	// Scope's detailed documetation
	/////////////////////////////////////////////
	std::string doc;

	/////////////////////////////////////////////
	//
	/////////////////////////////////////////////
	Scope *parentScope;

	/////////////////////////////////////////////
	// Supers of the scope. They're used to generate
	// scope's inheritance section in the HTML file.
	/////////////////////////////////////////////
	std::vector <std::string> supers;

	/////////////////////////////////////////////
	// Data members. Their detailed documentation
	// is generated in the same HTML file.
	/////////////////////////////////////////////
	std::vector <Property> properties;

	/////////////////////////////////////////////
	// Methods of the scope. Their detailed documentation
	// is generated in the same HTML file.
	/////////////////////////////////////////////
	std::vector <Function> methods;

	/////////////////////////////////////////////
	// Enums of the scope. Their detailed documentation
	// is generated in the same HTML file.
	/////////////////////////////////////////////
	std::vector <Enum> enums;

	/////////////////////////////////////////////
	// Nested scopes of the scope. Their detailed documentation
	// is generated in separated HTML files.
	/////////////////////////////////////////////
	std::vector <Scope> scopes;
};

#endif //  DOXEN_SCOPE_HPP
