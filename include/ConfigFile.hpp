#ifndef DOXEN_CONFIG_FILE_HPP
#define DOXEN_CONFIG_FILE_HPP

#include <string>
#include <vector>

//////////////////////////////
// Singleton class to read the
// config file of the project.
//////////////////////////////
class ConfigFile
{
public:
	//////////////////////////////
	// Reads the config file of the project
	// and inits the static properties
	//
	// Returns true if the file was read.
	//////////////////////////////
	static bool read(std::string filename);

	//////////////////////////////
	// Holds project's name.
	// It's specified in the config file
	// by 'project-name: value'
	//////////////////////////////
	static std::string projectName;

	//////////////////////////////
	// The input directory. Every .h and
	// .hpp files inside this directory
	// will be used to create documentation.
	//
	// It's specified in the config file
	// by 'input-directory: value'
	//
	// The value must be a full path
	//////////////////////////////
	static std::string inputDirectory;

	//////////////////////////////
	// The output directory. It's where
	// all the doc files will be created.
	//
	// It's specified in the config file
	// by 'output-directory: value'
	//
	// The value must be a full path
	//////////////////////////////
	static std::string outputDirectory;

	//////////////////////////////
	// The stylesheet file which will
	// be included in each created html file
	//
	// It's specified in the config file
	// by 'stylesheet-filename: value'
	//
	// The value must be a full path
	//////////////////////////////
	static std::vector <std::string> stylesheets;

	//////////////////////////////
	// A html file which defines the header
	// of each html created file.
	//
	// It's specified in the config file
	// by 'header-filename: value'
	//
	// The value must be a full path
	//////////////////////////////
	static std::string header;

	//////////////////////////////
	// A html file which defines the footer
	// of each html created file.
	//
	// It's specified in the config file
	// by 'footer-filename: value'
	//
	// The value must be a full path
	//////////////////////////////
	static std::string footer;

	static std::vector <std::string> mdFiles;

private:
	//////////////////////////////
	// Default constructor
	//////////////////////////////
	ConfigFile();

	//////////////////////////////
	// Default destructor
	//////////////////////////////
	~ConfigFile();
};

#endif // DOXEN_CONFIG_FILE_HPP
