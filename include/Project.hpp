#ifndef DOXEN_PROJECT_HPP
#define DOXEN_PROJECT_HPP

#include "Enum.hpp"
#include "Link.hpp"
#include "Function.hpp"
#include "Property.hpp"
#include "Scope.hpp"
#include <vector>
#include <regex>

/////////////////////////////////////////////
// Holds all the enums, functions, variables,
// scopes (classes, structures) and links of
// the project to document. Also, has the
// regexs used to read the files.
/////////////////////////////////////////////
class Project
{
public:
	/////////////////////////////////////////////
	// Constructor. Initializes all the regexs.
	/////////////////////////////////////////////
	Project();

	/////////////////////////////////////////////
	// Reads a source file and creates the corresponding
	// documentation elements
	/////////////////////////////////////////////
	void readFile(const std::string &filename);

	void readMDFile(const std::string &filename);

	void propagateLinks();

	/////////////////////////////////////////////
	// Writes all the documentation elements into
	// html files.
	/////////////////////////////////////////////
	void writeHTMLDoc() const;

	/////////////////////////////////////////////
	// Writes a HTML file name 'api-reference.html'
	// which lists all the elements of the project.
	/////////////////////////////////////////////
	void writeAPIReferenceLists() const;

private:
	/////////////////////////////////////////////
	// Adds a enum with a given name, doc and block.
	//
	// block contains the code of enum's elements
	/////////////////////////////////////////////
	void addEnum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks);

	/////////////////////////////////////////////
	// Adds a variable with a given name, type and documentation.
	/////////////////////////////////////////////
	void addVariable(const std::string &name, const std::string &type, const std::string &doc);

	/////////////////////////////////////////////
	// Adds a function to functions
	/////////////////////////////////////////////
	void addFunction(const std::string &returnType, const std::string &name, const std::string &params, const std::string &extras,
					 const std::string &doc);

	/////////////////////////////////////////////
	// Adds a scope.
	//
	// Params:
	// + nameLine: is used to determine if it's
	// a struct or a class
	// + doc: documentation of the scope
	/////////////////////////////////////////////
	void addScope(const std::string &nameLine, const std::string &doc);

	/////////////////////////////////////////////
	// Adds a link.
	//
	// If there's currentScope, link's text is
	// currentScope's name + "::" + text param and
	// link's href is currentScope's name + ".html#" + text
	//
	// If there's not currentScope, link's text is
	// text and href is text + ".html"
	/////////////////////////////////////////////
	void addLink(const std::string &text);

	/////////////////////////////////////////////
	// Adds a type (enum, class, struct).
	//
	// Inserts into types.
	/////////////////////////////////////////////
	void addType(const std::string &type);

	/////////////////////////////////////////////
	// Enums of the project
	/////////////////////////////////////////////
	std::vector <Enum> enums;

	/////////////////////////////////////////////
	// Functions of the project
	/////////////////////////////////////////////
	std::vector <Function> functions;

	/////////////////////////////////////////////
	// Variables of the project
	/////////////////////////////////////////////
	std::vector <Property> variables;

	/////////////////////////////////////////////
	// Scopes of the project (classes and structures)
	/////////////////////////////////////////////
	std::vector <Scope> scopes;

	/////////////////////////////////////////////
	// Links of the project
	/////////////////////////////////////////////
	std::vector <Link> links;

	std::vector <HTMLFile> mdFiles;

	/////////////////////////////////////////////
	// Links to types defined in the project.
	//
	// Classes, structures and enums are pushed
	// here. These links are used to highlight types
	// in function and properties.
	//
	// A type is inserted when the analizer finds a
	// enum or a scope using addType().
	/////////////////////////////////////////////
	std::vector <Link> types;

	/////////////////////////////////////////////
	// Pointer to the current scope.
	//
	// It's nullptr by default but once a scope is
	// read, it changes so the next scopes, functions,
	// variables and enums are added to it instead
	// of the project.
	/////////////////////////////////////////////
	Scope *currentScope;

	/////////////////////////////////////////////
	// Matches documentation lines
	// ^(\t| )*//+ ?(.*)$
	/////////////////////////////////////////////
	std::regex docCommentRegEx;

	/////////////////////////////////////////////
	// Matches classes and structures name lines
	// (\t| )*(class|struct) (\w+)( : .*)?$
	/////////////////////////////////////////////
	std::regex scopeRegEx;

	/////////////////////////////////////////////
	// Matches access levels (private, public, protected)
	// (\t| )*(public|protected|private):(.*)
	/////////////////////////////////////////////
	std::regex accessLevelRegEx;

	/////////////////////////////////////////////
	// Matches variables and attributes
	// (\t| )*((\w+ )?(\w+::)?(\w+)( ?<.*>)?( ?(\*|&)? ?))(\w+);
	/////////////////////////////////////////////
	std::regex variableRegEx;

	/////////////////////////////////////////////
	// MAtches functions and methods
	// (\t| )*((.*? ?|~)(\w+)\((.*)\)(.*))
	/////////////////////////////////////////////
	std::regex functionRegEx;

	/////////////////////////////////////////////
	// Matches enums and enum classes
	// (\t| )*enum( class)? \w+).*
	/////////////////////////////////////////////
	std::regex enumRegEx;

	/////////////////////////////////////////////
	// Matches block end '};'
	// .*};.*
	/////////////////////////////////////////////
	std::regex blockEndRegEx;

	/////////////////////////////////////////////
	// Matches li elements in markdown language
	// (\*|\+|-|\d+\.) (.*)
	/////////////////////////////////////////////
	std::regex liRegEx;
};

#endif // DOXEN_PROJECT_HPP
