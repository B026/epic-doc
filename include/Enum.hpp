#ifndef DOXEN_ENUM_HPP
#define DOXEN_ENUM_HPP

#include "HTMLFile.hpp"
#include "Link.hpp"
#include <vector>
#include <string>

//////////////////////////////
// Class used to describes enums'
// documentation.
//
// It's defined by a name,
// a documentation text and a list of
// EnumElement.
//////////////////////////////
class Enum
{
public:
	//////////////////////////////
	// Constructor. Sets the name
	// and documentation of the enum.
	//////////////////////////////
	Enum(const std::string &name, const std::string &doc, const std::string &block, std::vector <Link> &globalLinks);

	//////////////////////////////
	// Gets enum's name
	//////////////////////////////
	const std::string & getName() const;

	//////////////////////////////
	// Adds a EnumElement which is
	// identified by a name, a value
	// and a string documentation.
	//////////////////////////////
	void addEnum(const std::string &name, const std::string &value, const std::string &doc);

	void propagateLinks(const std::vector <Link> &globalLinks);

	//////////////////////////////
	// Writes the documentation as
	// a HTMLElement into a HTMLFile
	//////////////////////////////
	void writeHTMLElement(HTMLFile &htmlFile) const;

	//////////////////////////////
	// Writes the documentation into
	// a HTMLFile
	//////////////////////////////
	void writeHTMLFile() const;

private:
	//////////////////////////////
	// Writes enums as a table
	// into a HTMLFile
	//////////////////////////////
	void writeHTMLEnums(HTMLFile &htmlFile) const;

	//////////////////////////////
	// Describes the documentation
	// of an enum element
	//////////////////////////////
	struct EnumElement
	{
		//////////////////////////////
		// Element's name
		//////////////////////////////
		std::string name;

		//////////////////////////////
		// The value of the element.
		// Can be empty
		//////////////////////////////
		std::string value;

		//////////////////////////////
		// The doc text of the element
		//////////////////////////////
		std::string doc;

		//////////////////////////////
		// Constructor. Sets name,
		// value and doc
		//////////////////////////////
		EnumElement(const std::string &name, const std::string &value, const std::string &doc);
	};

	//////////////////////////////
	// Enum's name. It's set in the constructor.
	//
	// See getName()
	//////////////////////////////
	std::string name;

	//////////////////////////////
	// The doc text of the enum.
	// It's set in the constructor.
	//////////////////////////////
	std::string doc;

	//////////////////////////////
	// List of EnumElement.
	// They're created in the constructor.
	//
	// See addEnum()
	//////////////////////////////
	std::vector <EnumElement> enums;
};

#endif // DOXEN_ENUM_DOC_HPP
