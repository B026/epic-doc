#ifndef DOXEN_HTML_FILE_HPP
#define DOXEN_HTML_FILE_HPP

#include "HTMLElement.hpp"
#include "Link.hpp"
#include <string>
#include <vector>

/////////////////////////////////////////////
// Represents a HTML file and it's used to write
// them. It has 2 main elements, html and body.
/////////////////////////////////////////////
class HTMLFile
{
public:
	/////////////////////////////////////////////
	// Constructor. Sets filename and initilizes
	// htmlElement, bodyElement and mainElement
	/////////////////////////////////////////////
	HTMLFile(const std::string &filename);

	/////////////////////////////////////////////
	// Creates a new HTML element and adds it to
	// mainElement. Element's name, innerHTML and
	// id can be specified as args.
	//
	// Returns a reference to the new element
	/////////////////////////////////////////////
 	HTMLElement & addElement(const std::string &elementName, const std::string &innerHTML = "", const std::string &id = "");

	/////////////////////////////////////////////
	// Creates a table html element with as many th
	// elements as headers you specify. It's added
	// to mainElement.
	//
	// Returns a reference to the table element
	/////////////////////////////////////////////
	HTMLElement & addTable(const std::vector <std::string> &headers);

	HTMLElement * getMainElement();

	void propagateLinks(const std::vector <Link> &globalLinks);

	/////////////////////////////////////////////
	// Writes the html file in filename direction
	/////////////////////////////////////////////
	void write() const;

private:
	/////////////////////////////////////////////
	// Specifies the full path where the file must
	// be written by calling write(). It's set in
	// the constructor.
	/////////////////////////////////////////////
	std::string filename;

	/////////////////////////////////////////////
	// html root element
	/////////////////////////////////////////////
	HTMLElement htmlElement;

	/////////////////////////////////////////////
	// body element. It's child of htmlElement and
	// each element you add using addElement() or
	// addTable() is added as child of this one.
	/////////////////////////////////////////////
	HTMLElement *mainElement;
};

#endif // DOXEN_HTML_FILE_HPP
