#ifndef DOXEN_MD_INTERPRETER_HPP
#define DOXEN_MD_INTERPRETER_HPP

#include <regex>
#include <string>
#include "HTMLElement.hpp"
#include "HTMLFile.hpp"

/////////////////////////////////////////////
// So simple markdown interpreter used to
// interpret detailed documentation.
//
// It just supports paragraphs, unordered and ordered
// lists and inline markdown elements like:
//
// + bold
// + italics
// + strike through
// + links
// + images
/////////////////////////////////////////////
class MDInterpreter
{
public:
	enum LineType
	{
		Empty,
		Paragraph,
		Li,
		Note
	};

	/////////////////////////////////////////////
	// Interprets a given string in md format and
	// appends the generated html elements into
	// _htmlElements_
	/////////////////////////////////////////////
	static void mdToHTML(const std::string &md, HTMLElement &htmlElement);

	static void mdFileToHTMLFile(const std::string &filename, HTMLFile &htmlFile);

private:
	/////////////////////////////////////////////
	// Initializes regexs
	/////////////////////////////////////////////
	MDInterpreter();

	/////////////////////////////////////////////
	// Adds a HTMLElement to *parent* param. Also,
	// interprets inline MD elements inside of *innerHTML*
	/////////////////////////////////////////////
	static void createElement(const std::string &elementName, HTMLElement &parent, std::string innerHTML);

	static HTMLElement * analizePrevLine(LineType *prevLineType, HTMLElement *parentElement, const std::string &innerHTML);

	/////////////////////////////////////////////
	// Gets the unique MDInterpreter object
	/////////////////////////////////////////////
	static MDInterpreter & getInstance();

	/////////////////////////////////////////////
	// Matches bold MD elements. Text between a pair
	// of 2 asterisks or underscores is taken as
	// bold text.
	/////////////////////////////////////////////
	std::regex boldRegEx;

	/////////////////////////////////////////////
	// Matches image MD elements. It starts with a
	// ! symbol, image's alt text goes inside [] and
	// image's src inside () in that order.
	/////////////////////////////////////////////
	std::regex imageRegEx;

	/////////////////////////////////////////////
	// Matches italic MD elements. Text between a pair of
	// a asterisk or a underscore is taken as italic text.
	/////////////////////////////////////////////
	std::regex italicsRegEx;

	/////////////////////////////////////////////
	// Matches li MD element lines. Use an asterisk,
	// a score or a plus sign for unordered list elements,
	// and a number followed by a dot and a space for
	// ordered list elements.
	//
	// The first element in the list determines if
	// the list is unordered or ordered.
	/////////////////////////////////////////////
	std::regex liRegEx;

	/////////////////////////////////////////////
	// Matches link MD elements. Link's text goes
	// inside [] and href inside () in that order.
	/////////////////////////////////////////////
	std::regex linkRegEx;

	/////////////////////////////////////////////
	// Matches strikethrough MD elements. Text between
	// a pair of 2 scores is taken as strikethrough text.
	/////////////////////////////////////////////
	std::regex strikethroughRegEx;
};

#endif // DOXEN_MD_INTERPRETER_HPP
