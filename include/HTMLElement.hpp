#ifndef DOXEN_HTML_ELEMENT_HPP
#define DOXEN_HTML_ELEMENT_HPP

#include <string>
#include <vector>

#include "Link.hpp"

//////////////////////////////
// Represents an html element
// which is described by a name,
// innerHTML, parent node, child
// nodes and a list of attributes.
//
// It's used internally to generate
// the output html files.
//////////////////////////////
class HTMLElement
{
public:
	//////////////////////////////
	// Constructor. Sets element's parent
	//////////////////////////////
	HTMLElement(HTMLElement *parent = nullptr);

	//////////////////////////////
	// Constructor. Sets element's parent,
	// name and innerHTML
	//////////////////////////////
	HTMLElement(const std::string &name, const std::string &innerHTML, HTMLElement *parent = nullptr);

	//////////////////////////////
	// Sets element's name
	//////////////////////////////
	void setName(const std::string &name);

	const std::string & getName() const;

	//////////////////////////////
	// Sets element's innerHTML
	//////////////////////////////
	void setInnerHTML(const std::string &innerHTML);

	std::string * getInnerHTML();

	//////////////////////////////
	// Sets an specified element's attribute
	// by its name.
	//
	// If the element does not have an
	// attribute with the given name, adds it.
	//////////////////////////////
	void setAttribute(const std::string &attributeName, const std::string &attributeValue);

	//////////////////////////////
	// Sets element's parent
	//////////////////////////////
	void setParent(HTMLElement *parent);

	//////////////////////////////
	// Gets element's parent
	//
	// **Returns** nullptr if it doesn't
	// have parent
	//////////////////////////////
	HTMLElement * getParent();

	//////////////////////////////
	// Adds a child element with a given
	// name and innerHTML.
	//
	// Returns a reference to the child element
	//////////////////////////////
	HTMLElement & addElement(const std::string &childName, const std::string &childInnerHTML = "");

	HTMLElement * getElement(const std::string &childName);
	std::vector <HTMLElement> getChildren();

	//////////////////////////////
	// Parses the element into a string.
	//
	// <name attributes>child nodes...</name>
	//////////////////////////////
	std::string toString(int tabLevel = 0) const;

	//////////////////////////////
	// Propagate the given links in
	// innerHTML (if it's a inner html node)
	// and in its children recursively.
	//////////////////////////////
	void propagateLinks(const std::vector <Link> &globalLinks);

private:
	//////////////////////////////
	// Represents an attribute of a
	// HTMLElement. It's defined by
	// a name and a value
	//////////////////////////////
	struct Attribute
	{
		//////////////////////////////
		// Attribute's name
		//////////////////////////////
		std::string name;

		//////////////////////////////
		// Attribute's value
		//////////////////////////////
		std::string value;

		//////////////////////////////
		// Constructor. Sets attribute's
		// name and value
		//////////////////////////////
		Attribute(const std::string &name, const std::string &value);
	};

	//////////////////////////////
	// Element's name.
	//
	// Can be set in the constructor
	// or using setName()
	//////////////////////////////
	std::string name;

	//////////////////////////////
	// Element's inner html.
	//
	// Can be set in the constructor
	// or using setInnerHTML()
	//////////////////////////////
	std::string innerHTML;

	//////////////////////////////
	// Element's attributes.
	//
	// Use setAttribute() to add a new
	// attribute or to change the
	// value of an existent
	//////////////////////////////
	std::vector <Attribute> attributes;

	//////////////////////////////
	// Element's parent. It's set
	// in the constructors or using
	// setParent()
	//////////////////////////////
	HTMLElement *parent;

	//////////////////////////////
	// Element's child elements
	//
	// Use addElement() to add a new
	// child.
	//////////////////////////////
	std::vector <HTMLElement> children;
};

#endif // DOXEN_HTML_ELEMENT_HPP
